# dependencies
import pytest

# local
from xymon_client import Ghost, Xymon
import xymon_client


class TestGhost:
    @staticmethod
    @pytest.mark.parametrize(
        "args, expect",
        (
            (
                ("host.example.net", "10.0.0.1", "9"),
                ("host.example.net", "10.0.0.1", 9),
            ),
            (
                ("host.example.net", "10.0.0.2", 42),
                ("host.example.net", "10.0.0.2", 42),
            ),
            (
                ("host.example.net", "10.0.0.3", 3.14),
                ("host.example.net", "10.0.0.3", 3),
            ),
            (
                ("host.example.net", "10.0.0.4", b"7"),
                ("host.example.net", "10.0.0.4", 7),
            ),
        ),
    )
    def test_success(args, expect):
        assert tuple(Ghost(*args)) == expect

    @staticmethod
    @pytest.mark.parametrize(
        "args",
        (
            ("host.example.net", "10.0.1.1", "a"),
            ("host.example.net", "10.0.1.2", "0x11"),
        ),
    )
    def test_fail_value(args):
        with pytest.raises(ValueError):
            Ghost(*args)

    @staticmethod
    @pytest.mark.parametrize(
        "args",
        (
            (),
            ("host.example.net",),
            ("host.example.net", "10.0.2.1"),
            ("host.example.net", "10.0.2.2", 1, "something"),
        ),
    )
    def test_fail_params(args):
        with pytest.raises(TypeError):
            Ghost(*args)

    @staticmethod
    @pytest.mark.parametrize(
        "args, expect",
        (
            (
                ("host.example.net", "10.0.3.1", 42),
                "host.example.net|10.0.3.1|42",
            ),
        ),
    )
    def test_str(args, expect):
        assert str(Ghost(*args)) == expect


class TestXymon:
    @staticmethod
    def test__init__1():
        xymon = Xymon()
        assert xymon.sender
        assert xymon.target
        assert type(xymon.target) is tuple
        assert len(xymon.target) == 2
        assert xymon.target[0]
        assert type(xymon.target[0]) is str
        assert xymon.target[1]
        assert type(xymon.target[1]) is int
        assert 0 < xymon.target[1] < 65536

    @staticmethod
    def test__init__2():
        target = "xymon.example.net"
        port = 616
        sender = "sender.example.net"
        xymon = Xymon(target, port, sender)
        assert xymon.sender == sender
        assert xymon.target == (target, port)

    @staticmethod
    def test__str__():
        target = "xymon.example.net"
        assert str(Xymon(target)) == target

    def test_headline(self, monkeypatch):
        sender = "sender.example.net"
        timestamp = "1970-01-01T01:00:00"
        monkeypatch.setattr(xymon_client, "strftime", lambda _: timestamp)
        assert Xymon(sender=sender).headline == f"Message generated by {sender} at {timestamp}"


@pytest.mark.parametrize(
    "args, expect",
    (
        (("foo",), "foo"),
        ((("foo",),), "foo"),
        ((42,), "42"),
        ((b"foo",), "foo"),
        ((bytearray(b"foo"),), "foo"),
        ((None,), "None"),
        ((("foo", "bar"), ":"), "foo:bar"),
        ((("foo", "bar"), "_-_"), "foo_-_bar"),
        (((str(_) for _ in range(10)),), "0,1,2,3,4,5,6,7,8,9"),
    ),
)
def test_joiniterable(args, expect):
    assert xymon_client.joiniterable(*args) == expect


@pytest.mark.parametrize(
    "args, expect",
    (
        (({"foo", "bar"}, "|"), {"foo", "bar"}),
        (({"a": "b", "c": "d"}, ":"), {"a", "c"}),
    ),
)
def test_joiniterable_unsorted(args, expect):
    assert set(xymon_client.joiniterable(*args).split(args[1])) == expect
