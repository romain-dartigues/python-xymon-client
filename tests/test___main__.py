from collections import namedtuple
import io

import pytest

from xymon_client.__main__ import KWArgs, OutputFormatter, XymonServer
from xymon_client.__main__ import get_parser


class File(io.StringIO):
    def clear(self):
        self.seek(0)
        return self.truncate()

    def seek_read(self, clear=False):
        self.seek(0)
        data = self.read()
        if clear:
            self.clear()
        return data


class TestXymonServer:
    @staticmethod
    @pytest.mark.parametrize(
        "args, as_tuple, as_str",
        (
            (["host.example.net", None], ("host.example.net", 1984), "host.example.net:1984"),
            (["example.net", 616], ("example.net", 616), "example.net:616"),
            (["x.example.net", "1"], ("x.example.net", 1), "x.example.net:1"),
        ),
    )
    def test_xymon_server(args, as_tuple, as_str):
        xs = XymonServer(*args)
        assert tuple(xs) == as_tuple
        assert str(xs) == as_str

    @staticmethod
    @pytest.mark.parametrize(
        "args, exception",
        (
            ([], TypeError),
            (["one"], TypeError),
            (["th", "re", "e"], TypeError),
            (["example.net", "7c0"], ValueError),
        ),
    )
    def test_raises(args, exception):
        with pytest.raises(exception):
            XymonServer(*args)


class TestKWArgs:
    @staticmethod
    @pytest.mark.parametrize("kwargs, expect", (({"foo": "bar", "x": (4, 2)}, "foo='bar', x=(4, 2)"),))
    def test_as_str(kwargs, expect):
        assert str(KWArgs(**kwargs)) == expect


class TestOutputFormatter:
    data_sample = {
        "a turple": namedtuple("Turple", ("foo", "bar"))("Hello", "World!"),
        "weirdness": [object, Exception],
        "some": ["list", {"set"}, ("and", "tuple")],
        "another": [1, "bite", True, False, None],
    }

    @staticmethod
    def test_formats():
        assert {"text", "json"}.issubset(OutputFormatter.formats()), "must support at least text and JSON"

    @staticmethod
    def test_fail():
        with pytest.raises(ValueError):
            OutputFormatter("unsupported format")

    @staticmethod
    def test_to_text_width(monkeypatch):
        data = " ".join(["Na"] + ["na"] * 12 + ["Batman!"])
        with File() as file:
            output_formatter = OutputFormatter("text", file)

            monkeypatch.delenv("COLUMNS", raising=False)
            output_formatter(data)
            result = file.seek_read(clear=True)
            assert data in result
            assert len(data.splitlines()) == 1

            monkeypatch.setenv("COLUMNS", "eighty")
            output_formatter(data)
            assert result == file.seek_read(clear=True)

            monkeypatch.setenv("COLUMNS", "3")
            output_formatter(data)
            assert len(file.seek_read().splitlines()) == 14

    def test_to_json(self):
        with File() as file:
            output_formatter = OutputFormatter("json", file)
            output_formatter(self.data_sample)
            assert file.seek_read() == (
                "{\n"
                '  "a turple": [\n    "Hello",\n    "World!"\n  ],\n'
                '  "another": [\n    1,\n    "bite",\n    true,\n    false,\n    null\n  ],\n'
                '  "some": [\n'
                '    "list",\n'
                "    \"{'set'}\",\n"
                '    [\n      "and",\n      "tuple"\n    ]\n'
                "  ],\n"
                '  "weirdness": [\n    "<class \'object\'>",\n    "<class \'Exception\'>"\n  ]\n'
                "}\n"
            )

    @staticmethod
    def test_to_yaml_as_json(monkeypatch):
        """Just to ensure that when PyYAML is not available, it falls back to JSON"""
        monkeypatch.setattr("xymon_client.__main__.YAML", None)
        with File() as file:
            output_formatter = OutputFormatter("yaml", file)
            output_formatter([{"foo": "bar"}])
            assert file.seek_read() == '[\n  {\n    "foo": "bar"\n  }\n]\n'

    def test_to_yaml(self):
        pytest.importorskip("yaml")

        with File() as file:
            output_formatter = OutputFormatter("yaml", file)
            output_formatter(self.data_sample)
            assert file.seek_read() == (
                "---\n"
                "a turple:\n"
                "  bar: World!\n  foo: Hello\n"
                "another:\n- 1\n- bite\n- true\n- false\n- null\n"
                "some:\n"
                "- list\n"
                "- !!set\n  set: null\n"
                "- - and\n  - tuple\n"
                "weirdness:\n- <class 'object'>\n- <class 'Exception'>\n"
                "...\n"
            )


class TestTheParser:
    def test_1(self):
        _, exclude = get_parser()
        assert exclude & {"noop"}

    @staticmethod
    @pytest.mark.parametrize(
        "action, hostname, port",
        (
            ["ping", "xymon.example.net", 61984],
            ["ping", "xymon.example.net", None],
        ),
    )
    def test_parse_get_server(action, hostname, port):
        parser, _ = get_parser()
        args = parser.parse_args(["-s", f"{hostname}:{port}" if port else hostname, action])
        assert args.action == action

        assert len(args.server) == 1
        server = args.server[0]
        assert server.hostname == hostname
        assert server.port == (port if port else 1984)

    @staticmethod
    @pytest.mark.parametrize(
        "servers",
        (
            [["xymon.example.net", 61984]],
            [["xym-a.example.net", 1], ["xym-b.example.net", 2]],
            [["xym-c.example.net", 3], ["xym-d.example.net"]],
            [["xym-e.example.net"], ["xym-f.example.net", 6]],
            [["xym-g.example.net", 7], ["xym-h.example.net"]],
            [["xym-i.example.net"], ["xym-j.example.net"], ["xym-k.example.net"]],
        ),
    )
    def test_parse_get_multiple_servers(servers):
        parser, _ = get_parser()
        args = parser.parse_args([f"-s{':'.join(map(str, server))}" for server in servers])

        assert len(args.server) == len(servers)
        for index, server in enumerate(servers):
            arg_server = args.server[index]
            assert arg_server.hostname == server[0]
            assert arg_server.port == (server[1] if len(server) == 2 else 1984)
