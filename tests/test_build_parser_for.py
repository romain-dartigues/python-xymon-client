"""

``build_parser_for`` is a relatively obscure function intending to make a
Python object usable from the command line interface by exposing its methods.

The :mod:`click` [#]_ is somewhat similar.

.. #: https://palletsprojects.com/p/click/

"""
# stdlib
import argparse

# dependencies
import pytest

# local
from xymon_client.__main__ import build_parser_for


@pytest.fixture()
def subparsers():
    yield argparse.ArgumentParser().add_subparsers()


class SampleObject:
    def foo(self):
        """public foo method

        long description
        """

    def _bar(self):
        """private method"""

    def foobar(self, foo, bar="default"):
        """public with arguments

        :param foo: something
        :param bar: something else with a default value
        """


def test_build_parser_for1(subparsers):
    assert not subparsers.choices
    build_parser_for(subparsers, SampleObject)
    assert subparsers.choices.keys() == {"foo", "foobar"}


def test_build_parser_for2(subparsers):
    assert not subparsers.choices
    build_parser_for(subparsers, SampleObject)
    assert len(subparsers._choices_actions) == 2
    choices_actions = {_.dest: _ for _ in subparsers._choices_actions}
    assert choices_actions["foo"].dest == "foo"
    assert choices_actions["foo"].help == "public foo method"


def test_build_parser_for3(subparsers):
    assert not subparsers.choices
    build_parser_for(subparsers, SampleObject)
    assert subparsers.choices["foo"].usage == "public foo method\n\n        long description"
