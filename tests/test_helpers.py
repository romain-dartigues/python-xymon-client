import pytest

from xymon_client import Xymon
from xymon_client.helpers import Color, Helper, color_map


class TestColor:
    @staticmethod
    @pytest.mark.parametrize(
        "args, expect",
        (
            (
                ("0", "zero"),
                {"value": 0.0, "name": "zero"},
            ),
        ),
    )
    def test_color(args, expect):
        color = Color(*args)
        assert color == expect["value"]
        assert color.name == expect["name"]
        assert str(color) == expect["name"]


@pytest.fixture()
def helper():
    """Initialize a default helper"""
    yield Helper(Xymon())


class TestHelper:
    @staticmethod
    @pytest.mark.parametrize(
        "hostname, testname",
        (
            ("www.intra.example.net", "http"),
            ("foo-bar.example.net", None),
            (None, "ping"),
            (None, None),
        ),
    )
    def test__init__(hostname, testname):
        xymon = Xymon("xymon.example.net")
        helper = Helper(xymon, hostname, testname)
        assert helper.defaults.get("hostname") == hostname
        assert helper.defaults.get("testname") == testname
        assert helper.data == ""
        assert helper.xymon is xymon

    @staticmethod
    @pytest.mark.parametrize(
        "other, expect",
        (
            ("", color_map["clear"]),
            ("&red something gone pear shaped\n", color_map["red"]),
            ("&green\nNow it's &yellow\nand &green again\n", color_map["yellow"]),
            ("&green &yellow &red &blue &purple &clear", color_map["red"]),
            ("&purple &blue", color_map["clear"]),
            ("something", color_map["clear"]),
        ),
    )
    def test_color(helper, other, expect):
        assert helper.data == ""
        helper += other
        assert helper.data == other
        assert helper.color == expect

    @staticmethod
    @pytest.mark.parametrize(
        "text, default, expect",
        (
            ("", None, []),
            ("", color_map["blue"], [color_map["blue"]]),
            ("&red something gone pear shaped\n", None, [color_map["red"]]),
            (
                "&green &yellow &red &blue &purple &clear",
                None,
                [color_map["green"], color_map["yellow"], color_map["red"], color_map["clear"]],
            ),
            ("&purple &blue", None, []),
        ),
    )
    def test_get_colors(helper, text, default, expect):
        assert helper.get_colors(text, default) == expect
