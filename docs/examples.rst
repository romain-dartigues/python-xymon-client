########
Examples
########

Main library
############

Queries against a single Xymon server
=====================================


.. code-block:: pycon

   >>> from xymon_client import Xymon
   >>> xymon = Xymon('xymonserver01.example.net')
   >>> response = xymon.ping()
   'xymond 4.3.18\n'

   >>> xymon.query('xymonserver02.example.net', 'fping')
   'green Message generated by xymonserver01.example.net at mercredi 12 octobre 2016, 08:42:36 (UTC+0200) (executed every 180 sec)\n'

   >>> xymon.ghostlist()
   [<Ghost:xyz.example.net[10.234.71.190] @ 2016-10-12 08:41:03), <Ghost:abc.example.net[10.234.71.189] @ 2016-10-12 08:40:35), <Ghost:Combo_xtradb[10.234.72.13] @ 2016-10-12 08:40:44), <Ghost:Combo_logs01Snalert01[10.234.72.13] @ 2016-10-12 08:40:44)]

   >>> xymon.drop('xyz.example.net', 'http')
   ''

Queries sample
--------------

.. code-block:: pycon

   >>> xymon = Xymon('xymonserver01.example.net')

   >>> # query the Xymon server for yellow alerts
   >>> # on hosts whose name starts with "www"
   >>> # and only for the fields: hostname, testname and color
   >>> xymon.xymondboard('host=^www color=yellow', 'hostname,testname,color')
   [['www-portal.example.net', 'info', 'yellow'], ['www02.example.com', 'http', 'yellow']]

   >>> # this works all the same:
   >>> xymon.xymondboard(
   >>>  fields=('hostname', 'testname', 'color'),
   ...  criteria={'host': '^www', 'color': 'yellow'})
   [['www-portal.example.net', 'info', 'yellow'], ['www02.example.com', 'http', 'yellow']]

Queries to multiple Xymon servers at once
=========================================

.. code-block:: pycon

   >>> from xymon_client.xymon import Xymons # note the subtle difference
   >>> xymon = Xymons(['xymonserver01.example.net', 'xymonserver02.example.net'])
   >>> response = xymon.ping()
   {<Xymon darkstar to xymonserver02.example.net:1984>: 'xymond 4.3.18\n',
    <Xymon darkstar to xymonserver01.example.net:1984>: 'xymond 4.3.28\n'}

Helpers
#######

.. code-block:: python

   from xymon_client import Xymon
   from xymon_client.helpers import *

   # create an object with default hostname and default service
   xc = Helper(Xymon('xymon.example.net'), 'www.intra.example.net', 'http')

   # add a message
   xc+= "I see something wrong, let's make it &yellow\n"
   xc+= 'Oops, now &red, something gone pear shaped...\n'

   # what is the current error level?
   xc.color # should be red

   # 1. send the content of the current buffer with text added at the end
   # 2. override the global color of the message (from red to yellow)
   # 3. clear the buffer
   xc.status('but it is not *that* bad', color=yellow)

   # now the buffer has been cleared

   # send another message, same hostname but different service name
   xc.status('do not shoot the messenger!', service='logs')

The CLI
#######

A command-line interface is also provided; see: ``xymon-client -h``
or ``python -m xymon_client -h``.

Example:

.. code-block:: console

   $ xymon-client -s xymon01.example.net query --hostname www-portal.example.net --testname info
   'yellow Message generated by c234d183-069b-447e-73ab-84d5 at 2019-01-29T16:14:01\n'

   $ xymon-client -sxmon-{1,2}.example.net query --hostname www.example.net --testname info
   {<Xymon darkstar to xmon-1.example.net:1984>: '',
    <Xymon darkstar to xmon-2.example.net:1984>: 'red Message generated on www-portal.example.net at Thu Sep 15 11:58:04 2022 (executed every 300 sec by Xymon Client)\n'}
