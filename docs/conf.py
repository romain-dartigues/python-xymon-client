# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

from pathlib import Path
from sys import path
path.append(str((Path(__file__).parent.parent / 'src').resolve(strict=True)))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Xymon client'
copyright = '2016, Romain Dartigues'
author = 'Romain Dartigues'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for autodoc -----------------------------------------------------
autodoc_default_options = {
    'members': True,
    'undoc-members': True,
    'private-members': True,
    'special-members': False,
    'inherited-members': True,
    'show-inheritance': True,
}

autoclass_content = 'both'
autodoc_member_order = 'bysource'

autodoc_mock_imports = [
    "yaml",
]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']

html_theme_options = {
    'page_width': 'auto',
    'body_max_width': 'auto',
}

html_css_files = [
    'custom.css',
]

# -- Options for intersphinx ------------------------------------------------
intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
}
intersphinx_cache_limit = -1
intersphinx_timeout = 5

